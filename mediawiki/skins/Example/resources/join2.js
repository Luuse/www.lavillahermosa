function joinTo(el, color, width) {
  el.hover(function() {

    var elHovered = $(this);

    if (!elHovered.hasClass('clicked')) {
      $('.clicked').removeClass('target clicked');
      el.addClass('target');
      el.attr('data-target', 'true');

      var canvasId = 'canvas_0';

      if ($('canvas').length > 0) {
        var theId = $('canvas').attr('id');
        var nb = parseInt(theId.substr(theId.indexOf("_") + 1));
        var nextNb = nb + 1;
        $('canvas').remove();
        canvasId = 'canvas_' + nextNb;
      }

      $('body').append('<canvas id="' + canvasId + '" width="' + $(window).width() + '" height="' + $(window).height() + '"></canvas>');

      $('canvas').css({
        position: 'fixed',
        zIndex: '10',
        'pointer-events': 'none',
        left: 0,
        top: 0,
      });

      var c = document.getElementById(canvasId);
      var ctx = c.getContext('2d');

      drawLines(el, elHovered, c, ctx, color, width);

      $('*').on('scroll', function() {
        if (el.hasClass('target')) {
          drawLines(el, elHovered, c, ctx, color, width);
        }
      })

      $(window).on('resize', function() {
        drawLines(el, elHovered, c, ctx, color, width);
      })

    }

  }, function() {
    var elHovered = $(this);
    var canvas = $('canvas');
    var stateClass = elHovered.attr('class');
    var stateTarget = elHovered.attr('data-attr');

    if (!elHovered.hasClass('clicked') && !elHovered.hasClass('clickedFirst') && stateTarget != 'true') {
      canvas.remove();
      $('.target').removeClass('target');
      el.removeAttr('data-attr');

    } else if (stateClass == 'clickedFirst') {

      var canvasId = canvas.attr('id');

      var c = document.getElementById(canvasId);
      var ctx = c.getContext('2d');

      drawLines(el, elHovered, c, ctx, color, width);


      elHovered.removeClass('clickedFirst').addClass('clicked');


    }
  })

  el.click(function() {
    if ($(this).attr('class').split(' ').pop() == 'clicked') {
      $(this).removeClass('clicked');
    } else {
      if (SECTIONS.hasClass('onward')) {
        if ($(this).parents('section').attr('class') != 'onward') {
          $(this).addClass('clickedFirst');
        } else {
          el.addClass('clicked');
        }
      }
    }
  })
}

function killJoinTo(canvas, el) {
  canvas.remove();
  $('.target').removeClass('target');
  el.removeClass('clicked');
}

function drawLines(el, elHovered, c, ctx, color, width) {

  var etalon = $('.etalon').height();

  var elPosX = [];
  var elPosY = [];
  var elWidth = [];
  var elHeight = [];
  var elType = [];
  var newEndEl;

  var thisWidth = elHovered.outerWidth();
  var thisHeight = elHovered.outerHeight();

  el.each(function() {
    var article = $(this).parents('article');
    if ($(this).offset().left <= 0 && article.length > 0 && article.attr('id') != elHovered.parents('article').attr('id') && !article.parents('section').hasClass('onward')) {
      newEndEl = article.find('h2');
      newEndEl.addClass('target');
      elPosX.push(newEndEl.offset().left);
      elPosY.push(newEndEl.offset().top);
      elWidth.push(newEndEl.outerWidth());
      elHeight.push(0);
    } else {
      elPosX.push($(this).offset().left);
      elPosY.push($(this).offset().top);
      elWidth.push($(this).outerWidth());
      elHeight.push($(this).outerHeight());
    }

    elType.push($(this).prop('tagName').toLowerCase());
  })

  ctx.clearRect(0, 0, c.width, c.height);


  for (var i = 0; i < elPosX.length; i++) {

    var startX = elHovered.offset().left;
    var startY = elHovered.offset().top;
    var endX = elPosX[i];
    var endY = elPosY[i];


    if (endX != startX && endX != 0) {

      if (endX < startX) {
        endX = endX + elWidth[i];
      } else {
        startX = startX + thisWidth;
      }

      if (elHovered.is('span') && thisHeight > etalon) {

        if (endX < startX && endY < startY) {
          startY = startY + thisHeight - etalon;
        } else if (endX > startX && endY > startY) {
          startY = startY + etalon;
        }

      } else if (elType[i] == 'span' && elHeight[i] > etalon) {
        if (endX > startX && endY > startY) {
          endY = endY + elHeight[i] - etalon;
        } else if (endX < startX && endY < startY) {
          endY = endY + etalon;
        }
      } else {
        if (endY < startY) {
          endY = endY + elHeight[i];
        } else {
          startY = startY + thisHeight;
        }

      }

      var cp1X = startX;
      var cp1Y = endY / 2;
      var cp2X = endX;
      var cp2Y = endY / 2;

      ctx.beginPath();
      ctx.moveTo(startX, startY);
      ctx.bezierCurveTo(cp1X, cp1Y, cp2X, cp2Y, endX, endY);
      ctx.stroke();
      ctx.strokeStyle = color;
      ctx.lineWidth = width;
      ctx.stroke();

    }
  }
}