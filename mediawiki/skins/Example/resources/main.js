var body = document.querySelector("body");
var fragments = document.querySelectorAll(
  "#content p > a, #content p > span a"
);
var breadCrumb = document.getElementById("breadcrumb");
var httpRequest;
var grid = document.querySelector("#grid");
var imgs = document.querySelectorAll("p img");
var i = 0;

grid.addEventListener("click", function () {
  grid.querySelector(".grid").classList.toggle("visible");
});

addLegendToImg(imgs);
preventClick(fragments);
fragmentClick(fragments);

function fragmentClick(fragments) {
  fragments.forEach((fragment) => {
    fragment.addEventListener("click", function (event) {
      var fragmentUrl = fragment.getAttribute("href");
      var anchor = fragmentUrl.split("#")[1];

      makeRequest(fragmentUrl, fragment, anchor);
    });
  });
}

function makeRequest(url, node, anchor) {
  httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    alert("Abandon :( Impossible de créer une instance de XMLHTTP");
    return false;
  }

  if (node.getAttribute("href").indexOf("mailto") > -1) {
  } else {
    httpRequest.onreadystatechange = function () {
      showContent(node, anchor);
    };
    httpRequest.open("GET", url);
    httpRequest.responseType = "html";
    httpRequest.send();
  }
}

function showContent(node, anchor) {
  if (httpRequest.readyState === XMLHttpRequest.DONE) {
    if (httpRequest.status === 200) {
      var articles = document.querySelectorAll("#mw-wrapper");
      var oldArticle = node.closest('#mw-wrapper');
      var newArticle = document.createElement("article");
      var newArticleId;
      newArticle.innerHTML = httpRequest.responseText;
      newArticle = newArticle.querySelector("#mw-wrapper");

      articles.forEach(function (article) {
        article.classList.remove("focus");
      });
      newArticle.classList.add("focus");


      if (anchor) {
        newArticle.classList.add("anchor");
        newArticleId = "article-" + i;
        newArticle.classList.add(newArticleId);
        insertAfter(newArticle, oldArticle);
        var targetedFragment = newArticle.querySelector("span#" + anchor);
        targetedFragment.classList.add('active');
        node.classList.add('active');
        var active = $('.active');
        i++;
      // alignFragments(node, newArticle, targetedFragment);
      } else {
        insertAfter(newArticle, node);
      }

      feedBreadCrumb(node.getAttribute("href"), node.getAttribute("title"), newArticleId);

      var newFragments = newArticle.querySelectorAll(
        "#content p > a, #content p > span a"
      );

      var newImgs = newArticle.querySelectorAll("p img");
      
      addLegendToImg(newImgs);
      preventClick(newFragments);
      fragmentClick(newFragments);
      focusContent(newArticle);
      joinTo(active, 'red', 1)
      styleArticles(); //in progress
    } else {
      alert("Il y a eu un problème avec la requête.");
    }
  }
}

function preventClick(els) {
  els.forEach((el) => {
    el.addEventListener("click", function (event) {
      if (el.getAttribute("href").indexOf("mailto") > -1) {
      } else {
        event.preventDefault();
      }
    });
  });
}

function insertAfter(newNode, existingNode) {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}

function feedBreadCrumb(url, txt, id) {
  breadCrumb.innerHTML += " -> <a class='crumb' id='" + id + "' href='" + url + "'>" + txt + "</a>";
  var crumbs = document.querySelectorAll('.crumb');
  preventClick(crumbs);
  clickOnBreadCrumb(crumbs);
}

function clickOnBreadCrumb(crumbs) {
  crumbs.forEach(crumb => {
    crumb.addEventListener('click', function(el) {
      var crumbId = crumb.getAttribute("id");
      var target = document.querySelector('.' + crumbId);
      focusContent(target)
      document.querySelector('.focus').classList.remove('focus');
      target.classList.add('focus');
    })
  })
}

function offset(el) {
  var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
}

function addLegendToImg(imgs) {
  imgs.forEach((img) => {
    var legend = img.getAttribute("alt");
    if (legend) {
      var legendEl = document.createElement("div");
      legendEl.innerHTML = legend;
      insertAfter(legendEl, img);
    }
  });
}

function styleArticles() {
  var articles = document.querySelectorAll('#mw-wrapper');
  for (let i = 0; i < articles.length; i++) {
    const article = articles[i];
    // article.style.fontSize = i+5*i + 'px';
  }
}

function focusContent(article) {
  var posLeft = 200;
  var articleW = offset(article).left;
  $('html,body').animate({scrollLeft: articleW - posLeft}, 50);
  // window.scrollBy({
  //   top: 0,
  //   left: articleW - posLeft,
  //   behavior: 'smooth'
  // });

}
