Fonts

## Meta Old French.

Différentes fontes pour des types de texte:
* Tons -> 
  * texte long, 
  * texte descriptif rapide
* texte académique
* texte brouillon
* Article NYtimes.

Opérationnels:

* metaOldFrench_legend (<img>+<div>)
* metaOldFrench_poem (<poem>)
* metaOldFrench_title (<h2>,<h3>,<h4>,<h5>)
* metaOldFrench_italic (<i>, <em>)
* metaOldFrench_bold (<b>, <strong>)
* metaOldFrench_code (<code>)
* metaOldFrench_citation (<q>)
* metaOldFrench_extract (<blockquote>)
