  // PBCKCODE CUSTOMIZATION
  config.pbckcode = {
    // An optional class to your pre tag.
    cls: '',

    // The syntax highlighter you will use in the output view
    highlighter: 'PRETTIFY',

    // An array of the available modes for you plugin.
    // The key corresponds to the string shown in the select tag.
    // The value correspond to the loaded file for ACE Editor.
    modes: [['HTML', 'html'], ['CSS', 'css'], ['PHP', 'php'], ['JS', 'javascript']],


    // The theme of the ACE Editor of the plugin.
    theme: 'chaos',

    // Tab indentation (in spaces)
    tab_size: '4'
  };

  // modes: [
  //   ['C/C++'        , 'c_cpp'],
  //   ['C9Search'     , 'c9search'],
  //   ['Clojure'      , 'clojure'],
  //   ['CoffeeScript' , 'coffee'],
  //   ['ColdFusion'   , 'coldfusion'],
  //   ['C#'           , 'csharp'],
  //   ['CSS'          , 'css'],
  //   ['Diff'         , 'diff'],
  //   ['Glsl'         , 'glsl'],
  //   ['Go'           , 'golang'],
  //   ['Groovy'       , 'groovy'],
  //   ['haXe'         , 'haxe'],
  //   ['HTML'         , 'html'],
  //   ['Jade'         , 'jade'],
  //   ['Java'         , 'java'],
  //   ['JavaScript'   , 'javascript'],
  //   ['JSON'         , 'json'],
  //   ['JSP'          , 'jsp'],
  //   ['JSX'          , 'jsx'],
  //   ['LaTeX'        , 'latex'],
  //   ['LESS'         , 'less'],
  //   ['Liquid'       , 'liquid'],
  //   ['Lua'          , 'lua'],
  //   ['LuaPage'      , 'luapage'],
  //   ['Markdown'     , 'markdown'],
  //   ['OCaml'        , 'ocaml'],
  //   ['Perl'         , 'perl'],
  //   ['pgSQL'        , 'pgsql'],
  //   ['PHP'          , 'php'],
  //   ['Powershell'   , 'powershel1'],
  //   ['Python'       , 'python'],
  //   ['R'            , 'ruby'],
  //   ['OpenSCAD'     , 'scad'],
  //   ['Scala'        , 'scala'],
  //   ['SCSS/Sass'    , 'scss'],
  //   ['SH'           , 'sh'],
  //   ['SQL'          , 'sql'],
  //   ['SVG'          , 'svg'],
  //   ['Tcl'          , 'tcl'],
  //   ['Text'         , 'text'],
  //   ['Textile'      , 'textile'],
  //   ['XML'          , 'xml'],
  //   ['XQuery'       , 'xq'],
  //   ['YAML'         , 'yaml']
  // ],
