var fragments = document.querySelectorAll("main section a");
preventClick(fragments);
fragmentClick(fragments);
addCodeElement();


function addCodeElement() {
  var pres = document.querySelectorAll('pre');
  pres.forEach(pre => {
    var preContent = pre.innerHTML;
    pre.innerHTML = '';
    var newCode = document.createElement('code');
    newCode.innerHTML = preContent;
    pre.appendChild(newCode);
  })
  codeClass(pres);
}

function codeClass(pres) {
  pres.forEach(pre => {
    var theClass = pre.getAttribute('class');
    var code = pre.querySelector('code');
    code.classList.add('language-' + theClass);
  })
  hljs.highlightAll();
}

function fragmentClick(fragments) {
  fragments.forEach((fragment) => {
    fragment.addEventListener("click", function () {
      if (!fragment.classList.contains('disabled')) {
        var fragmentUrl = fragment.getAttribute("href");
        fragment.classList.add('disabled');
        makeRequest(fragmentUrl, fragment);
      }
    });
  });
}

function waypoint(fragment, id) {
  var parent = fragment.closest('section')
  var waypoint = new Waypoint({
    element: document.getElementById(id),
    handler: function() {
      // moveContent(fragment, true);
      var crumbs = document.querySelectorAll('.crumb');
      crumbs.forEach(crumb => {
        if(crumb.getAttribute('data-id') == id) {
          focusCrumb(crumbs, crumb)
        }
      })
    }
  })
  var waypointBack = new Waypoint({
    element: document.getElementById(id),
    offset: function () {
      return this.context.innerHeight() - this.adapter.outerHeight() - window.innerHeight
    },
    handler: function() {
      console.log('yo ')
      // moveContent(parent, false);
    }
  })
}

function makeRequest(url, node) {
  httpRequest = new XMLHttpRequest();
  if (!httpRequest) {
    alert("Abandon :( Impossible de créer une instance de XMLHTTP");
    return false;
  }
  httpRequest.onreadystatechange = function () {
    showContent(node);
  };
  httpRequest.open("GET", url);
  httpRequest.responseType = "html";
  httpRequest.send();
}

function showContent(node) {
  if (httpRequest.readyState === XMLHttpRequest.DONE) {
    if (httpRequest.status === 200) {
      var articles = document.querySelectorAll('section');
      var temp = document.createElement('div');
      temp.innerHTML = httpRequest.responseText;
      var oldArticle = node.closest('section');
      var depth = parseInt(oldArticle.getAttribute('data-depth'));
      var newArticle = temp.querySelector('section');
      articles.forEach(article => {
        article.classList.remove('focus');
      })
      newArticle.classList.add('focus');
      var newArticleId = newArticle.getAttribute("id");
      var newFragments = newArticle.querySelectorAll("a");
      insertAfter(newArticle, node);
      newArticle.setAttribute('data-depth', depth + 1);
      feedBreadCrumb(oldArticle.getAttribute('id'), newArticleId, newArticle.getAttribute("data-title"));
      preventClick(newFragments);
      fragmentClick(newFragments);
      waypoint(newArticle, newArticleId);
      addCodeElement();
    }
  }
}

function feedBreadCrumb(parentId, id, txt) {
  var breadCrumb = document.getElementById("breadcrumb");
  var lis = breadCrumb.querySelectorAll('li');
  lis.forEach(li => {
    var dataId = li.getAttribute('data-id');
    console.log(dataId)
    console.log(parentId)
    if (dataId == parentId) {
      var newCrumb = document.createElement('ul');
      insertAfter(newCrumb, li);
      newCrumb.innerHTML += "<li class='crumb' data-id='" + id + "'><a href='#" + id +"'>" + txt + "</a></a>";
    }
  })
  var crumbs = breadCrumb.querySelectorAll('.crumb');
  clickOnbreadcrumb(crumbs);
}

function clickOnbreadcrumb(crumbs) {
  crumbs.forEach(crumb => {
    crumb.addEventListener('click', function() {
      focusCrumb(crumbs, crumb);
    })
  })
}

function focusCrumb(crumbs, crumb) {
  crumbs.forEach(crumb => {
    crumb.classList.remove('focus');
  })
  crumb.classList.add('focus');
}

function moveContent(fragment, more) {
 var depth = parseInt(fragment.getAttribute('data-depth'));
 var main = document.querySelector('main');
 var margin = more == true ? -((depth - 1) * 32) : -((depth + 1) * 32);
 main.style.marginLeft = margin + 'px';
}

function preventClick(els) {
  var page = window.location || window.document.location;
  var path = page.pathname.split('/')[1]
  if (els.length == undefined) {
    var href = els.getAttribute("href");
    els.addEventListener("click", function (event) {
      (href.indexOf(path) == 1) ? event.preventDefault() : null;
    });
  } else {
    els.forEach((el) => {
      var href = el.getAttribute("href");
      el.addEventListener("click", function (event) {
        (href.indexOf(path) == 1) ? event.preventDefault() : null;
      });
    });
  }

}

function insertAfter(newNode, existingNode) {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}

function clickOnInfo() {
  var buttonInfo = document.querySelector('footer h1');
  var infos = document.querySelector('.textInfo');
  buttonInfo.addEventListener('click', function() {
    infos.classList.toggle('visible')
  })
  infos.addEventListener('click', function() {
    infos.classList.remove('visible');
  })
}

clickOnInfo();
