
<?php 
  include('./inc/header.php');
  $uniqId = uniqid ('article-', true); 
?>
<main>
  <section class="article article-<?= $page->id ?>" id="<?= $uniqId ?>" data-title="<?= $page->title ?>">
    <div class="content"><?=$page->text ?></div>
  </section>
</main>
<?php
  include('./inc/nav.php');
  include('./inc/footer.php');