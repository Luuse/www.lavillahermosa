<?php
  include("./inc/header.php");
?>

<main>
  <section class="article" id="<?= $uniqId ?>" data-depth="0">
    <h1><?= $page->title ?></h1>
    <div class="content"><?=$page->text ?></div>
  </section>
</main>

<?php
  include("./inc/footer.php");
  include("./inc/nav.php");
