    <footer>
      <?php
        $info = $pages->get('template=colophon');
        include('./../colophon.php');
      ?>
    </footer>
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.0.1/build/highlight.min.js"></script>
    <script src="<?php echo $config->urls->templates?>/scripts/libs/noframework.waypoints.min.js"></script>
    <script src="<?php echo $config->urls->templates?>/scripts/main.js"></script>
  </body>

</html>
