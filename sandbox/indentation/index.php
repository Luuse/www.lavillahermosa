<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style/main.css">
</head>
<body>
    <h1>La villa Hermosa</h1>
    <section>
        <?php
            include('inc/content.php');

            function putSections($nb, $randSection, $p, $count) {
                $count ++;
                $randNb = rand(0, 10);
                $randSection = rand(0, $randNb);
                for ($i=0; $i <= $nb; $i++) { 
                    echo '<p>' . $p[rand(0, 3)] . '</p>';
                    if ($i == $randSection) {
                        echo '<section>';
                        if ($count < 100) {
                            putSections($nb, $randSection, $p, $count);
                        }

                        echo '</section>';
                    }
                }
            }
            
            $randNb = rand(0, 10);
            $randSection = rand(0, $randNb);
            $count = 0;
            putSections($randNb, $randSection, $paragraphs, $count);
        ?>
    </section>
 
</body>
</html>