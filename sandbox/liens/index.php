<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LV(M)H</title>
    <link rel="stylesheet" href="assets/style/main.css">
</head>
<body>
  <h1>La villa Hermosa</h1>
  <?php
    $articles = array();
    $dir= 'assets/articles';
    $MyDirectory = opendir($dir) or die('Erreur');
    while($Entry = readdir($MyDirectory)) {
      if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
      } elseif ($Entry !='.' && $Entry != '..') {
        array_push($articles,$Entry);
      }
    }
    closedir($MyDirectory);

    sort($articles);

    foreach ($articles as $article) {
      include($dir . '/' . $article);
    }

  ?>
<script src="assets/scripts/main.js"></script>
</body>
</html>