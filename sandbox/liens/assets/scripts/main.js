var fragments = document.querySelectorAll('a');
var articles = document.querySelectorAll('article');

preventClick(fragments)

showLink(fragments)

function showLink(fragments) {

  fragments.forEach(fragment => {

    fragment.addEventListener("click", function(event){
      

      var fragmentLink = fragment.getAttribute('href');
      var isSeedPeer = fragmentLink.split('#')[1] ? true : false;

      articles.forEach(article => {
        
        var articleId = article.getAttribute('id');
        fragmentLink = (isSeedPeer == true) ? fragmentLink.split('#')[0] : fragmentLink;
        if (articleId == fragmentLink) {
          var articlesClonedPrev = (isSeedPeer == true) ? article.querySelectorAll('article') : '' ;
          if (articlesClonedPrev) {
            console.log(articlesClonedPrev);
            articlesClonedPrev.forEach(articleClonedPrev => {
              articleClonedPrev.remove();
            }) 
          }
          var articleCloned = article.cloneNode(true);
          insertAfter(articleCloned, fragment);
          articleCloned.classList.add('show')
          var newFragments = articleCloned.querySelectorAll('a');
          showLink(newFragments);
          preventClick(newFragments);
        }
      })

    });

  });
}


function preventClick(els) {
  els.forEach(el => {
    el.addEventListener("click", function(event){
     event.preventDefault();
    });
  });
}

function insertAfter(newNode, existingNode) {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}