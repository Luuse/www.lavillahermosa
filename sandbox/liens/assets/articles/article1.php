<article id="a1">
  <h1>Article1</h1>
  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nihil suscipit, ipsa voluptate delectus dolore non placeat quasi quidem dolorem voluptatibus tempora atque distinctio veniam nulla debitis deleniti quos, eum iure.</p>
  <p>Lorem ipsum dolor sit amet <a id="f1" href="a2" class="seed">consectetur</a> adipisicing elit. A autem iusto nulla eveniet odit quasi enim veniam. Labore, odit qui eligendi illum temporibus natus voluptates! Fugit aliquam voluptatibus quam qui.</p>
  <p>Sapiente debitis ea quas at quidem! <a id="f2" href="a2#f1" class="seedPeer">Consequatur sapiente libero</a> obcaecati neque nemo aliquid pariatur dolore porro voluptatibus officiis, veritatis illum qui fugiat, veniam et, culpa nisi aspernatur? Totam, corrupti unde!
  Aliquam doloribus sint laudantium, quam iure quibusdam tempore inventore delectus? Omnis reiciendis qui ducimus? <a id="f3" href="a3" class="seed">Adipisci</a> excepturi qui voluptas minima voluptatum reiciendis. Incidunt ab pariatur ex omnis, quo cupiditate expedita dolor.
  <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo eveniet neque maxime quae officiis asperiores adipisci vitae, quibusdam at vel, voluptatem esse harum ea, eos repellat error eaque sed deleniti.</p>
  <p><a id="f2" href="a4#f2" class="seedPeer">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis amet commodi sed ipsum voluptatum voluptate placeat, est, iusto sunt ullam aliquam numquam tempore eligendi ab quos deleniti dolores perferendis vel?</a></p>
</article>